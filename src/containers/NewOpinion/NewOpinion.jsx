import { Component } from 'react';
import './NewOpinion.scss';

const INITIAL_FORM = {
    name: '',
    stars: '',
    description: '',
};

class NewOpinion extends Component{
 
    state= INITIAL_FORM;

    handleFormSubmit = ev => {
        ev.preventDefault();
        this.props.createOpinion(this.state);
        this.setState(INITIAL_FORM);
    }

    onChangeInput = ev => {
        const { name, value } = ev.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }



    render() {
        console.log(this.state);
        return(
            
            <div className="first_container_newjob">
                <h3 className="addJobExperience">➕ Añadir opinión</h3>
                    <form onSubmit={this.handleFormSubmit}>
                        <label>
                            <p>Nombre</p>
                            <input type="text" name="name" value={this.state.name} onChange={this.onChangeInput} />
                        </label>

                        <label>
                            <p>Estrellas</p>
                            <input type="text" name="stars" value={this.state.stars} onChange={this.onChangeInput} />
                        </label>

                        <label>
                            <p>Descripción</p>
                            <textarea type="text" name="description" value={this.state.description} onChange={this.onChangeInput} />
                        </label>

                        <div className="button_send">
                            <button type="submit">Añadir comentario</button>
                                
                        </div>


                    </form>
            </div>
        )
    }

}

export default NewOpinion;