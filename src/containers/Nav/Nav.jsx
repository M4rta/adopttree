import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSeedling, faHome, faLeaf, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import './Nav.scss';

const Nav = () => {

    return(
        <nav className="navbar">
            <div className="navbar__left">
                <img src="logo_adoptree.png" alt="logo" className="navbar__left_logo"/>
            </div>

            <div className="navbar__right">
                <FontAwesomeIcon className="navbar__right__item" icon={faHome} />
                <Link to="/" className="navbar__right__item">HOME</Link>
               
            </div>

            <div className="navbar__right">
                <FontAwesomeIcon  className="navbar__right__item" icon={faLeaf} />
                <Link to="/adopt" className="navbar__right__item">ADOPTA</Link>
                
            </div>
            
            <div className="navbar__right">
                <FontAwesomeIcon className="navbar__right__item" icon={faSeedling} />
                <Link to="/transplantit" className="navbar__right__item">¡TRASPLÁNTALO!</Link>
            </div>

            <div className="navbar__right">
                <FontAwesomeIcon  className="navbar__right__item" icon={faEnvelope} />
                <Link to="/register" className="navbar__right__item">REGISTRO</Link>
            </div>


        </nav>
    )

}


export default Nav;
