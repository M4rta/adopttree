import React, { Component } from  'react';

import './Card.scss';

class Card extends Component {

    render() {
        return(
            <div className="card">
                    {this.props.elements.map(el => {
                            return (
                                <div className="card__container">
                            
                                <div className="card__container__img" key={el.name}>
                                    <img src={el.image} alt="Parques" className="card__container__img__imgpark"/>


                                    <div className="card__container__img__imgpark__items">

                                        <h3 className="card__container__img__imgpark__items__h3">Nombre:</h3>
                                        <p className="card__container__img__imgpark__items__p">{el.name}</p>

                                        <h3 className="card__container__img__imgpark__items__h3">Descripción:</h3>
                                        <p className="card__container__img__imgpark__items__p">{el.description}</p>

                                        <h3 className="card__container__img__imgpark__items__h3">Dirección de adopción:</h3>
                                        <p className="card__container__img__imgpark__items__p">{el.addres}</p>

                                    </div>
                                
                                </div>
                            </div>
                                
                            );
                        })}

                </div>
                    
            
                
            
        )
    }

    

}


export default Card;