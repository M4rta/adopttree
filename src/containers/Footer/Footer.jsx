import React from 'react';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faInstagram } from '@fortawesome/free-solid-svg-icons';
import './Footer.scss';

const Footer = () => {

    return(
        <footer className="footer">
            <div className="footer__first_container">

                <div className="footer__first_container__left">

                    <ul className="footer__first_container__left_ul1">
                        <li className="footer__first_container__left_ul1__item">© Marta González Rothemund</li>

                        <li className="footer__first_container__left_ul1__item">Instagram: @may_emm_93</li>

                        <li className="footer__first_container__left_ul1__item">LinkedIn: Marta González Rothemund</li>

                    </ul>

                </div>


                <div className="footer__first_container__right">

                    <ul className="footer__first_container__right_ul2">

                        <li className="footer__first_container__right_ul2__item">Datos de interés </li>
                        <li className="footer__first_container__right_ul2__item">*La presente web es ficticia y en ningún momento las direcciones de locales o lugares públicos son reales o se encuentran en algún convenio o acuerdo con la misma.</li>
                        <li className="footer__first_container__right_ul2__item">Contacta: martagr93@gmail.com</li>


                    </ul>

                </div>
                
            </div>

        
        </footer>
    )

}


export default Footer;