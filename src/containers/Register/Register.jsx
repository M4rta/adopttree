import React, { Component } from 'react';
import './Register.scss';


//Contraseña con 8 caracteres, mayúscula, minúscula y número
const INITIAL_STATE = {
    username: '',
    email: '',
    password: '',
}

class Register extends Component {

    state = INITIAL_STATE;

    handleSubmitForm = async ev => {
        ev.preventDefault();

        this.props.submitRegister(this.state);
        this.setState(INITIAL_STATE);
    }

    handleChangeInput = (ev) => {

        const { name, value } = ev.target;
        this.setState({ [name]: value })
    }
    render() {
        return(
        
            <div className="form">
    
                <div className="form__container">
                    <form onSubmit={this.handleSubmitForm} className="form__container__register">
                        <label htmlFor="username" className="form__container__register_label">
                            <p className="form__container__register_p">Nombre de usuario</p>
                            <input
                                type="text"
                                name="username"
                                value={this.state.username}
                                onChange={this.handleChangeInput}
                            />
                        </label>
    
                        <label htmlFor="email" className="form__container__register_label">
                            <p className="form__container__register_p">Email</p>
                            <input
                                type="text"
                                name="email"
                                value={this.state.email}
                                onChange={this.handleChangeInput}
                            />
                        </label>
    
                        <label htmlFor="password" className="form__container__register_label">
                            <p className="form__container__register_p">Contraseña</p>
                            <input
                                type="password"
                                name="password"
                                value={this.state.password}
                                onChange={this.handleChangeInput}
                            />
                        </label>
    
                        <div>
                            <button type="submit">Registrar</button>
                        </div>
    
                    </form>
    
                </div>
    
            </div>
        )
    }
    

}


export default Register;