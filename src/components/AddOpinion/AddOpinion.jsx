import React, { Component } from 'react';
import './AddOpinion.scss';

class AddOpinion extends Component {
    render() {

        const {
            name,
            stars,
            description,
        } = this.props.addOpinions;

        return(
            <div>
				<p className="opinionName">{name}</p>
				<h3 className="opinionStar">{stars}</h3>
                <p className="opinionDescription">{description}</p>
			</div>
        )
    
    }
}

export default AddOpinion;