import React from 'react';
import './Home.scss';

const Home = () => {

    return(
        <div className="home">

            <div className="home__container">
                <h1 className="home__container__h1">¡Hola! Bienvenid@ a Adopt-Tree</h1>

                <p className="home__container__p">Esta web surge con la idea de la importante labor de la reforestación y de la introducción de las plantas en nuestra vida cotidiana. ¿Sabes la importante labor que realizan?</p>

                <div className="home__container__p__image">

                    <p className="home__container__p__image__text">Aparte de ser     la principal fuente de Oxígeno en nuestro   planeta, también contribuyen a la limpieza y  purificación del aire, mantienen el suelo,   regulan la humedad y contribuyen a la     estabilidad del clima, entre otras muchas cosas.    Además, brindan cobijo a multitud de otros seres   vivos -aves, insectos, pequeños mamíferos, otras  plantas, hongos, etc-.</p>
                </div>

                <p className="home__container__p">Otro aporte MUY importante de la flora de nuestro planeta es que suele ser la principal fuente de alimento: tanto vegetal como animal. ¡Piensalo! La mayoría de la carne que consumimos depende, en gran parte, del sustento que les proporcionan las plantas, árboles y cereales, ya que la carne que solemos consumir procede de animales herbívoros. No se nos olvide también que nuestra medicina actual viene de su gran ancestro: la Fitoterapia; o comúnmente conocida como medicina a base de plantas.</p>

                <h3 className="home__container__h3">¡Únete a nuestra comunidad verde!</h3>

                <p className="home__container__p">La principal función y utilidad de esta web es poder llevar el mundo verde a tu casa: que aprendas a cómo tratar una planta, cómo cuidarla y regarla; y, en su última finalidad, poder aportar ese granito de arena que tanta falta nos hace: ¡la repoblación de espacios verdes! Qué mejor plan para un fin de semana que hacer una escapadita con tu familia, amigos, pareja, o incluso con tu deslumbrante persona, a un garn parque al aire libre: trasplantar a tu pequeña amiga y darte un festín con un buen picnic. No solo estás ayudando al medioambiente, sino al pequeño negocio, ya que la tramitación de la adopción de la planta se realiza en pequeños comercios a precios más asequibles que por una simple compra de plantas/ flores.</p>
                

            </div>



        </div>
    )

}


export default Home;
