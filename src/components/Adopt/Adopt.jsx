import React from 'react';
import Card from '../../containers/Card';
import './Adopt.scss';

const data = {

    plantasAdoptadas:[
        {
            id: 1,
            image: "https://upload.wikimedia.org/wikipedia/commons/6/62/Cheiranthus_cheiri_L_orange_bedder_dsc00903.jpg",
            name: "Alhelí",
            description: "Flor de esta planta, sencilla o doble, de colores variados -blanco, amarillo, rosa o púrpura- y muy aromática.",
            addres: "Avda. del Pueblo Paleta, 25."
        },
  
        {
            id: 2,
            image: "https://www.dhresource.com/0x0/f2/albu/g5/M00/51/60/rBVaJFltvZCAKRtBAAFiSBCyd1c588.jpg/wedding-flowers-real-touch-white-anemones.jpg",
            name: "Anémona",
            description: "La anémona era una flor cargada de simbolismo en la Grecia antigua y donde su nombre significa «la hija del viento».",
            addres: "Calle de la Ciudad Verde, 42, Bajo."
        },
        {
            id: 3,
            image: "https://www.floresyplantas.net/wp-content/uploads/calas-de-colores-1.jpg",
            name: "Alcatraz Cala",
            description: "Zantedeschia aethiopica, conocida comúnmente como alcatraz, cala, cala de Etiopía, aro de Etiopía, lirio de agua, de la familia de las aráceas, la más robusta y ampliamente naturalizada del género Zantedeschia.",
            addres: "Camino Pewter City, 125."
        },
  
        {
            id: 4,
            image: "https://www.flores.ninja/wp-content/uploads/2016/11/Gardenia-e1480049259128.jpg",
            name: "Gardenia",
            description: "Las gardenias son unas plantas de la familia de las rubiáceas originarias de China. El nombre científico de la especie más común es Gardenia jasminoides. Hay 259 especies descritas y, de estas, solo 134 aceptadas.",
            addres: "Calle Lavender Town, 130"
        },
        {
            id: 5,
            image: "https://infinitanaturaleza.com/wp-content/uploads/2020/11/sauce-lloron-11.jpg",
            name: "Sauce",
            description: "Género compuesto de unas 400 especies​ de árboles y arbustos caducifolios, se distribuyen principalmente por las zonas frías y templadas del Hemisferio Norte, con preferencia por tierras húmedas.",
            addres: "Avda. Isla Canela, SN"
        },
  
        {
            id: 6,
            image: "https://www.guiadejardineria.com/wp-content/uploads/2017/03/cultiva-moras-en-maceta-01.jpg",
            name: "Morera",
            description: "Morus alba, comúnmente morera, es una especie de árbol perteneciente al género Morus, familia de las moráceas.",
            addres: "Calle Saffron City, 3"
        }
  
    ]
  
    
  }

const Adopt = () => {

    return(
        <div className="adopt">

            <div className="adopt__container">
                <h1 className="adopt__container__h1">Adopta a tu amiga verde</h1>

                <p className="adopt__container__p">En esta sección podrás encontrar una lista de todas las plantas, árboles y flores que puedes adoptar y en qué local más cercano a tu domicilio poder recoger e iniciar los trámites de adopción por un pequeño aporte económico. También tienes señalado qué planta es apta para su trasplante.</p>

                <p className="adopt__container__p">Reserva tu planta marcando la casilla, ¡que no se te adelanten!</p>


                <div>
                    
                    <Card elements={data.plantasAdoptadas}/>

                </div>

  
            </div>

            



        </div>
    )

}


export default Adopt;