import React, { Component } from 'react';
import Parks from '../Parks/Parks';
import AddOpinion from '../AddOpinion';
import Opinion from '../Opinion';
import NewOpinion from '../../containers/NewOpinion';
import './TransplantIt.scss';

const dataPark = {
    parques:[
        {
            id: 1,
            image: "https://ciudadlineal.net/assets/images/image_5dfe7797488b9_retiro.jpg",
            name: "El Retiro",
            description: "Con 125 hectáreas y más de 15000 árboles, el parque de El Retiro es un remanso verde en el centro de Madrid. Especial atención merecen algunos de sus jardines: el jardín de Vivaces, los jardines de Cecilio Rodríguez, los jardines del Arquitecto Herrero Palacios, la Rosaleda y el Parterre Francés con el ahuehuete, el árbol más antiguo de Madrid, del que se dice que podría tener alrededor de 400 años.",
            addres: "Plaza de la Independencia, 7"
        },
  
        {
            id: 2,
            image: "https://aws.traveler.es/prod/designs/v1/assets/2000x1333/141111.jpg",
            name: "El Capricho",
            description: "Estamos ante uno de los espacios verdes que conforman el patrimonio artístico-natural de Madrid. Situado en Alameda de Osuna, es uno de los parques más bellos y, paradójicamente, de los más desconocidos (sobre todo su búnker de la Guerra Civil)​. Creado en 1784 por los Duques de Osuna y especialmente por la duquesa, doña María Josefa de la Soledad Alonso Pimentel. Protectora de artistas, toreros e intelectuales, la duquesa creó un auténtico paraíso natural que frecuentaron las personalidades más ilustres de la época y en el que trabajaron los artistas, jardineros y escenógrafos con más prestigio.",
            addres: "Paseo de la Alameda de Osuna, 25"
        },
        {
            id: 3,
            image: "https://guias-viajar.com/madrid/wp-content/uploads/2017/05/madrid-rosaleda-parque-oeste-001.jpg",
            name: "Parque del Oeste y Rosaleda",
            description: "El Jardín de La Rosaleda de Madrid goza de una ubicación privilegiada dentro del Parque del Oeste, uno de los espacios verdes más importantes de la ciudad. Está abierto todos los días del año, la entrada es libre para todos los públicos y no es necesario hacer reservas. Se encuentra muy cerca de la estación de Príncipe Pío, de la Plaza de España, el Templo de Debod y del Teleférico.",
            addres: "Calle de la Rosaleda, 1"
        }
  
    ],

    opinions:[
        {
            id: 1,
            name: "Verónica",
            stars: "⭐⭐⭐⭐",
            description: "¡Muy interesante esta web! Propone actividades muy divertidas y sanas al aire libre, que tanta falta hace en estos momentos. Además sienta bien ayudar al medio ambiente 🙂"
        },

        {
            id: 2,
            name: "David",
            stars: "⭐⭐",
            description: "Un poco rara la web. Quiero decir, ¿quién querría adoptar una planta pudiendo comprarla? 🤷‍♂️ Por lo demás muy chula, el diseño es bonito. Las excursiones merecen la pena 👌"
        },

        {
            id: 3,
            name: "Marichi",
            stars: "⭐⭐⭐⭐⭐",
            description: "¡¡Me encanta!! Adopté una planta en la floristería de mi calle 😍 Es una orquídea preciosa 🥰 Recomendé la web a mis amigos por la idea tan novedosa y todos ellos han adoptado plantas y árboles aptos para trasplantar. Pasamos una tarde genial llevándolos al parque y luego con las excurisiones que ofrecen 💪 ¡¡La recomiendo!! 🙌"
        },

        {
            id: 4,
            name: "Javi",
            stars: "⭐⭐⭐",
            description: "Muy buena la web 🙂 La recomiendo, la idea muy original 👍"
        } 
  
    ]
}

class TransplantIt extends Component {


    state = {
        addOpinionList: []
    };



    addUserOpinion = addOpinions => {
        this.setState({ addOpinionList: [...this.state.addOpinionList, addOpinions] });
    }
    render() {
        return(
            <div className="container__information">
    
                    <h1 className="container__information__h1">Los mejores lugares para trasplantar a tu compi verde</h1>
    
                    <p className="container__information__p">Te presentamos los mejores lugares al aire libre donde poder hacer una pequeña excursión para darle una nueva vida a tu querida planta/ árbol.</p>
    
                    <p className="container__information__p">Contamos con una gran comunidad de voluntarios que te podrán ofrecer un montón de actividades: excursiones, rutas de senderismo, picnics, juegos al aire libre, jincanas... ¡Además! Sólo por ayudar a reforestar la zona, recibirás un regalito* muy especial. ¿A qué esperas?</p>
    
                
    
                <div>
                    <Parks elements={dataPark.parques}/>
                </div>
    
    
                <div>

                    <Opinion elements={dataPark.opinions}/>

                    <div>
                    {this.state.addOpinionList.map(addOpinions => (
                            <div>
                            <AddOpinion key={`${addOpinions.name} - ${addOpinions.stars} - ${addOpinions.description}`} addOpinions = {addOpinions}/>
                            </div> 
                                 
                            )
                        )}
                    </div>
                    
                    
                </div>
    
                <div>
                    <NewOpinion createOpinion= {this.addUserOpinion}/> 
                </div>
    
    
    
            </div>
        )
    
    }
    }
    
    


export default TransplantIt;