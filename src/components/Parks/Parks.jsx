import React, { Component } from 'react';
import './Parks.scss';

class Parks extends Component {

    render(){
        return(
            <div className="container">

                
                    <h2 className="container__h2">Parques públicos</h2>

                        {this.props.elements.map(el => {
                                return (
                                    <div className="container__card">
                            
                                        <div className="container__card__img" key={el.name}>
                                            <img src={el.image} alt="Parques" className="container__card__img__imgpark"/>


                                            <div className="container__card__img__imgpark__items">

                                                <h3 className="container__card__img__imgpark__items__h3">Nombre:</h3>
                                                <p className="container__card__img__imgpark__items__p">{el.name}</p>

                                                <h3 className="container__card__img__imgpark__items__h3">Descripción:</h3>
                                                <p className="container__card__img__imgpark__items__p">{el.description}</p>

                                                <h3 className="container__card__img__imgpark__items__h3">Dirección:</h3>
                                                <p className="container__card__img__imgpark__items__p">{el.addres}</p>

                                            </div>
                                        
                                        </div>
                            </div>
                                    
                                );
                            })}
                




            </div>
        )
    }

}


export default Parks;