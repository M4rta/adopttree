import React, { Component } from 'react';
import './Opinion.scss';

class Opinion extends Component {

    render(){
        return(
            <div className="container">

                
                    <h2 className="container__h2">¡Deja tu opinión sobre nuestra web!</h2>

                    <p className="container__p">Tu opinión nos importa y nos ayuda a mejorar. Deja tu comentario sobre nuestros servicios y excursiones.</p>

                        {this.props.elements.map(el => {
                                return (
                                    <div className="container__opinion">
                            
                                        <div className="container__opinion__items" key={el.name}>


                                            <div className="container__opinion__items">

                                                <h3 className="container__opinion__items__h3">Nombre:</h3>
                                                <p className="container__opinion__items__p">{el.name}</p>

                                                <p className="container__opinion__items__p">{el.stars}</p>

                                                <h3 className="container__opinion__items__h3">Comentario:</h3>
                                                <p className="container__opinion__items__p">{el.description}</p>

                                            </div>
                                        
                                        </div>
                                    </div>
                                    
                                );
                            })}
                




            </div>
        )
    }

}


export default Opinion;