import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Nav from './containers/Nav';
import Home from './components/Home';
import Adopt from './components/Adopt';
//import Card from './containers/Card';
import TransplantIt from './components/TransplanIt';
import Register from './containers/Register';
import Footer from './containers/Footer';
import './App.css';




class App extends Component {

  submitRegister = form => {
    console.log('DATOS PARA EL REGISTRO', form);
  }


  render() {
    return (
      <Router>
        <div className="App">

        <header className="App__header">
            <Nav/>
          </header>
          
          <div>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/adopt" exact component={Adopt} />
              <Route path="/transplantit" exact component={TransplantIt} />
              <Route path="/register" exact component={Register} />
              
            </Switch>
          </div>
          
    
          <div className="App__body">
    
            
    
            
    
          </div>
    
          <footer className="App__footer">
            <Footer/>
    
          </footer>
    
    
    
        </div>
      </Router>
      );
  }
  
}

export default App;
